
# coding: utf-8

# ## Import Libraries
# Import the necessary Python libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import json
import ast
import math


# to check if the value is not a number (python)
def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


# ## Load Data
# Load credits csv file and perform two tasks while loading:
# 
# Convert all columns with json data as json type

def load_credits_data(file_path):
    df = pd.read_csv(file_path, dtype='unicode')
    # all json columns`
    json_columns = ['cast', 'crew']
    for column in json_columns:
        # use ast because json data has single quotes in the csv, which is invalid for a json object; it should be " normally
        df[column] = df[column].apply(lambda x: np.nan if pd.isnull(x) else ast.literal_eval(x))
       
#        for(int i=0; i<df[column].size; i++) {
#            a = df[column][i];
#            if pd.isnull(a) {
#                a = np.nan
#            } else {
#                a = ast.literal_eval(a)
#            }
#            df[column][i] = a;
#        }

    return df


# Load the credits data csv file
credits = load_credits_data(r"dataset/credits.csv")


# Let's see the basic summay of the available datset
print "before flattening"
print "shape ", credits.shape
print "columns ", credits.columns
print "info ", credits.info()
print "null sum ", credits.isnull().sum()

# Flattening is converting the json into required columns 

# Create an empty DateFrame credits_flattened to store the de-serialized output. Flatten the actors, directors, producers, casting crew's name and gender information
# class pandas.DataFrame(data=None, index=None, columns=None, dtype=None, copy=False)
credits_flattened = pd.DataFrame(None,None,columns=['movie_id','actor_1_gender','actor_2_gender','actor_3_gender','actor_4_gender','actor_5_gender','actor_1_name','actor_2_name','actor_3_name','actor_4_name','actor_5_name','director_gender','director_name','producer_gender','producer_name','casting_gender','casting_name'])

for i,row in credits.iterrows():
    if i%1000 == 0:
        print "processing " + str(i) + "th row"
    # dummy row
    newrow = {'movie_id':np.nan,'actor_1_gender':np.nan,'actor_2_gender':np.nan,'actor_3_gender':np.nan,'actor_4_gender':np.nan,'actor_5_gender':np.nan,'actor_1_name':np.nan,'actor_2_name':np.nan,'actor_3_name':np.nan,'actor_4_name':np.nan,'actor_5_name':np.nan,'director_gender':np.nan,'director_name':np.nan,'producer_gender':np.nan,'producer_name':np.nan,'casting_gender':np.nan,'casting_name':np.nan}
    
    # fill movie id
    newrow['movie_id'] = int(row['id'])
    
    # fill cast
    count=1
    for item in row['cast']:
        # if we have 5 actors, we are done, and we exit the loop
        if count==6:
            break
        if 'gender' in item and 'name' in item:
            newrow['actor_'+str(count)+'_gender'] = item['gender']
            newrow['actor_'+str(count)+'_name'] = item['name']
            count += 1

    # fill crew
    director=0
    producer=0
    casting=0
    for item in row['crew']:
        # if we have 1 director, 1 producer, 1 casting director, we are done, exit the loop
        if director and producer and casting:
            break
        if 'job' in item and item['job'] in ['Director','Producer','Casting'] and 'gender' in item and 'name' in item:
            if item['job'] == 'Director' and not director:
                newrow['director_gender'] = item['gender']
                newrow['director_name'] = item['name']
                director =  1
            elif item['job'] == 'Producer' and not producer:
                newrow['producer_gender'] = item['gender']
                newrow['producer_name'] = item['name']
                producer =  1
            elif item['job'] == 'Casting' and not casting:
                newrow['casting_gender'] = item['gender']
                newrow['casting_name'] = item['name']
                casting =  1

    credits_flattened = credits_flattened.append(newrow,ignore_index=True)


# Let's verify the structure of the flattened credits data
print "After flattening"
print "shape ", credits_flattened.shape
print "head ", credits_flattened.head()


# Save the flattened credits data to a csv file
credits_flattened.to_csv('flattened.csv',index=False)


# Function to load movies data. This function converts date column to datetime.date data type and also assign json data type to corresponding json columns
def load_movies_metadata(file_path):
    df = pd.read_csv(file_path, dtype='unicode')
    # covert each item of release_date to datetime.date type entity
    #df['release_date'] = pd.to_datetime(df['release_date'], errors='coerce').apply(lambda x: x.date())
    # all json columns`
    json_columns = ['belongs_to_collection', 'genres', 'production_companies', 'production_countries', 'spoken_languages']
    for column in json_columns:
        # use ast because json data has single quotes in the csv, which is invalid for a json object; it should be " normally
        df[column] = df[column].apply(lambda x: np.nan if pd.isnull(x) else ast.literal_eval(x))
    return df


# Load movies metadata using function defined above
movies = load_movies_metadata(r"dataset/movies_metadata.csv")


# Verify movies metadata information
print "movies head", movies.head(3)
print "movies columns", movies.columns


# Create a new dataframe to store only the desired columns from the movies metadata loaded from csv
#movies_required = movies[['budget', 'genres', 'id', 'imdb_id', 'original_language', 'title', 'popularity', 'release_date', 'revenue','vote_average','vote_count']]
movies_required = movies[['budget', 'genres', 'id', 'imdb_id', 'original_language', 'title', 'popularity', 'revenue','vote_average','vote_count']]

print "movies req head ", movies_required.head()
print "movies req shape ", movies_required.shape


# Keep only the first genre information out of all the json values
for i,row in movies_required.iterrows():
    if len(row[1])>0:
        if 'name' in row[1][0]:
            row[1] = row[1][0]['name']

print "movies req head ", movies_required.head()

# Rename columns appropriately
movies_required.rename(columns={'genres':'genre_top','id':'movie_id'}, inplace=True)

print "movies req head ", movies_required.head()


# Below columns had to be dropped because the order of values in these rows was incorrect
movies_required.drop(movies_required.index[19730],inplace=True)
movies_required.drop(movies_required.index[29502],inplace=True)
movies_required.drop(movies_required.index[35585],inplace=True)


# Convert movie_id data type to numeric value instead of the default 'string' for merging
print "movies req head ", movies_required.head()

# Merge the two dataframes to get the desired result
merged_df = pd.merge(credits_flattened,movies_required,on=['movie_id'])

#merged_df = pd.read_csv(r"flattened_merged.csv", dtype='unicode')
merged_df[['movie_id']] = merged_df[['movie_id']].astype(float)

print "merged shape ", merged_df.shape
print "merged columns ", merged_df.columns
print "merged head \n", merged_df.head()

def load_ratings_data(file_path):
    df = pd.read_csv(file_path, dtype='unicode')
    
    df_req = df[['movieId', 'rating']]
    #df_req[['movie_id']] = df[['movieId']].astype(float)
    df_req[['movie_id', 'rating']] = df[['movieId', 'rating']].astype(float)
    
    #df_req = df_req.apply(pd.to_numeric).astype(float)
    df_final = df_req['rating'].groupby(df_req['movie_id']).mean().reset_index()
    #groupby_movies.mean().reset_index()
    
    #df_req.groupby('movieId').mean()
    
    return df_final

ratings = load_ratings_data(r"dataset/ratings.csv")
print ratings.head()

merged_dfr = pd.merge(merged_df,ratings,on=['movie_id'])

# Write the merged result to a csv file
merged_dfr.to_csv('flattened_merged_rating.csv',index=False)
'''
'''
merged_dfr = pd.read_csv(r"flattened_merged.csv", encoding='utf-8')

# get dataframe for first 3 actors of each movie
actor1 = merged_dfr[['actor_1_name', 'vote_average']]
actor2 = merged_dfr[['actor_2_name', 'vote_average']]
actor3 = merged_dfr[['actor_3_name', 'vote_average']]

# change columns name of actor_<id>_name to actor
newcolumns = ['actor', 'vote_average']
actor1.columns = newcolumns
actor2.columns = newcolumns
actor3.columns = newcolumns

#actors_name = pd.concat([actor1[['actor']], actor2[['actor']], actor3[['actor']]])
#actors_name.drop_duplicates(inplace=True)
#actors_name = actors_name.reset_index()

# Merge the dataframe of 3 actors from each movie to single dataframe
actors = pd.concat([actor1, actor2, actor3])
actors = actors.reset_index()

# get dataframe for directors for each movie
director = merged_dfr[['director_name', 'vote_average']].reset_index()

# get dataframe for producers for each movie
producer = merged_dfr[['producer_name', 'vote_average']].reset_index()

# compute average ratings for actors, directors, producers
actor_avg = actors['vote_average'].groupby(actors['actor']).mean().reset_index()
director_avg = director['vote_average'].groupby(director['director_name']).mean().reset_index()
producer_avg = producer['vote_average'].groupby(producer['producer_name']).mean().reset_index()

# replace actor_1_name column with average rating of each actor from actor_avg dataframe
merged_dfr['actor_1_name'] = merged_dfr['actor_1_name'].apply(lambda x : 5.0 if pd.isnull(x) else (actor_avg.loc[actor_avg.actor == x])[['vote_average']].values[0,0] )
print "actor1 replaced with average ratings"

# replace actor_2_name column with average rating of each actor from actor_avg dataframe
merged_dfr['actor_2_name'] = merged_dfr['actor_2_name'].apply(lambda x : 5.0 if pd.isnull(x) else (actor_avg.loc[actor_avg.actor == x])[['vote_average']].values[0,0] )
print "actor2 replaced with average ratings"

# replace actor_3_name column with average rating of each actor from actor_avg dataframe
merged_dfr['actor_3_name'] = merged_dfr['actor_3_name'].apply(lambda x : 5.0 if pd.isnull(x) else (actor_avg.loc[actor_avg.actor == x])[['vote_average']].values[0,0] )
print "actor3 replaced with average ratings"

# replace producer_name column with average rating of each actor from actor_avg dataframe
merged_dfr['producer_name'] = merged_dfr['producer_name'].apply(lambda x : 5.0 if pd.isnull(x) else (producer_avg.loc[producer_avg.producer_name == x])[['vote_average']].values[0,0] )
print "producer replaced with average ratings"

# replace director_name column with average rating of each actor from actor_avg dataframe
merged_dfr['director_name'] = merged_dfr['director_name'].apply(lambda x : 5.0 if pd.isnull(x) else (director_avg.loc[director_avg.director_name == x])[['vote_average']].values[0,0] )
print "director replaced with average ratings"

# Save dataframe with replaced names
merged_dfr.to_csv('flattened_merged_vote.csv',index=False)

# convert language column to integer 
# 1 : if language is english
# 0 : otherwise
merged_dfr['original_language'] = merged_dfr['original_language'].apply(lambda x: 1.0 if x=='en' else 0.0)

# convert genre to one-hot representation matrix
genre = pd.get_dummies(merged_dfr['genre_top'])
# drop the column representing empty genre list from datasets
genre.drop(['[]'],axis=1,inplace=True)

# drop named genre column from training dataset
merged_dfr.drop(['genre_top'],axis=1,inplace=True)
# add one-hot represented genre to training dataset
merged_dfr = pd.concat([merged_dfr, genre], axis=1)

merged_dfr.to_csv('flattened_merged_vote.csv',index=False)

merged_dfr = pd.read_csv(r"flattened_merged_vote.csv", encoding='utf-8')

# Select columns to be used finally for training
print merged_dfr.columns
traincolumns = [u'movie_id', u'actor_1_gender', u'actor_2_gender', u'actor_3_gender', u'actor_1_name', u'actor_2_name', u'actor_3_name', u'director_gender', u'director_name', u'producer_gender', u'producer_name', u'budget', u'original_language', u'popularity', u'revenue', u'vote_average', u'Action', u'Adventure', u'Animation', u'Comedy', u'Crime', u'Documentary', u'Drama', u'Family', u'Fantasy', u'Foreign', u'History', u'Horror', u'Music', u'Mystery', u'Romance', u'Science Fiction', u'TV Movie', u'Thriller', u'War', u'Western']

# Save training training in train.csv trimmed using above column list
train_final = merged_dfr[traincolumns]
train_final.to_csv('train.csv',index=False)

train_final = pd.read_csv(r"train.csv", encoding='utf-8')

# round float numbers to 2 decimals values
train_final = np.round(train_final, decimals=2)

# check for columns with empty cells
print train_final.isnull().sum()

# fill empty gender values to gender not specified represented by 0.0
# 1.0 for female
# 2.0 for male
train_final['actor_1_gender'] = train_final['actor_1_gender'].fillna(0.0)
train_final['actor_2_gender'] = train_final['actor_2_gender'].fillna(0.0)
train_final['actor_3_gender'] = train_final['actor_3_gender'].fillna(0.0)
train_final['director_gender'] = train_final['director_gender'].fillna(0.0)
train_final['producer_gender'] = train_final['producer_gender'].fillna(0.0)

# fill rest empty values as 5.0
train_final = train_final.fillna(5.0)
train_final.to_csv('train_final.csv',index=False)
