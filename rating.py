
# coding: utf-8

# ## Import Libraries
# Import the necessary Python libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import json
import ast

# ## Load Data
# Load credits csv file and perform two tasks while loading:
# 
# Convert all columns with json data as json type

def load_ratings_data(file_path):
    df = pd.read_csv(file_path, dtype='unicode')
    
    df_req = df[['movieId', 'rating']]
    df_req[['movie_id']] = df[['movieId']].astype(int)
    df_req[['rating']] = df[['rating']].astype(float)
    
    #df_req = df_req.apply(pd.to_numeric).astype(float)
    df_final = df_req['rating'].groupby(df['movie_id']).mean().reset_index()
    #groupby_movies.mean().reset_index()

    #df_req.groupby('movieId').mean()
    
    return df_final


# Load the credits data csv file
ratings = load_ratings_data(r"dataset/ratings.csv")


# Let's see the basic summay of the available datset
print "shape ", ratings.shape
print "columns ", ratings.columns
print "info ", ratings.info()


# Write the merged result to a csv file
ratings.to_csv('rating_by_movies.csv',index=False)


# We shall use this csv file for our modelling purposes.
