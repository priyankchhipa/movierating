
# coding: utf-8

# ## Import Libraries
# Import the necessary Python libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import json
import ast

from sklearn import preprocessing
from sklearn.svm import SVR
from sklearn.cross_validation import train_test_split
from sklearn.metrics import mean_squared_error

data = pd.read_csv('train_final.csv', dtype='unicode')
svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1,max_iter=50000,verbose=1)
svr_lin = SVR(kernel='linear', C=1e3,max_iter=50000,verbose=1)
svr_poly = SVR(kernel='poly', C=1e3, degree=2,max_iter=50000,verbose=1)

labels = data['vote_average']
train1 = data.drop(['vote_average', 'movie_id'], axis=1)

x_train, x_test, y_train, y_test = train_test_split(train1, labels, test_size=0.10, random_state=2)

scaler = preprocessing.StandardScaler().fit(x_train)
x_train_scaled = scaler.transform(x_train)

svr_rbf_clf = svr_rbf.fit(x_train, y_train)

y_pred = svr_rbf_clf.predict(x_test)
testScore_rbf = mean_squared_error(y_pred, y_test)
#testScore_rbf = svr_rbf_clf.score(x_test, y_test)
print testScore_rbf


svr_lin_clf = svr_lin.fit(x_train, y_train)

y_pred = svr_lin_clf.predict(x_test)
testScore_lin = mean_squared_error(y_pred, y_test)
#testScore_rbf = svr_rbf_clf.score(x_test, y_test)
print testScore_lin


svr_poly_clf = svr_poly.fit(x_train, y_train)

y_pred = svr_poly_clf.predict(x_test)
testScore_poly = mean_squared_error(y_pred, y_test)
#testScore_rbf = svr_rbf_clf.score(x_test, y_test)
print testScore_poly

'''
testScore_rbf = svr_rbf_clf.score(x_test, y_test)
print testScore_rbf
testScore_lin = svr_lin_clf.score(x_test, y_test)
print testScore_lin
testScore_poly = svr_poly_clf.score(x_test, y_test)
print testScore_poly
'''