
# coding: utf-8

# ## Import Libraries
# Import the necessary Python libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import json
import ast

from sklearn import preprocessing
from sklearn.cross_validation import train_test_split
from sklearn.metrics import mean_squared_error

from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn import tree

import pickle as pkl

# read training data from train_final.csv
data = pd.read_csv('train_final.csv', dtype='unicode')

# Loading Initial regression models from sklearn module
## Linear Regression
## Support Vector Regression
## Decision Tree

# Load Linear Regression
reg = LinearRegression()

# Load Support Vector Regression with RBF Kernel
svr_rbf = SVR(kernel='rbf', C=1e2, gamma=0.1, max_iter=100000, verbose=1)
# Load Support Vector Regression with Linear Kernel
svr_lin = SVR(kernel='linear', C=1e2, max_iter=100000, verbose=1)
# Load Support Vector Regression with Polynomial Kernel
svr_poly = SVR(kernel='poly', C=1e2, degree=2, max_iter=100000, verbose=1)

# Load Decision Tree Regression 
dtree = tree.DecisionTreeRegressor()

# Divide set into X=train1 and Y=labels
labels = data['vote_average']
train1 = data.drop(['vote_average', 'movie_id', 'revenue'], axis=1)

# Split total data into training and testing data 
# Split the data into 95:5 ratio
x_train, x_test, y_train, y_test = train_test_split(train1, labels, test_size=0.05, random_state=2)
y_test = y_test.astype(float)

# Learn the normalizer using the total available data
scaler = preprocessing.Normalizer().fit(train1)

# Normalize the training and testing data using above trained scaler 
x_train_scaled = scaler.transform(x_train)
x_test_scaled = scaler.transform(x_test)

# Save dataset to pickle for testing use
dataset = [x_train_scaled, x_test_scaled, y_train, y_test]
with open('train_data/dataset.pkl', 'wb') as output:
    pkl.dump(dataset, output)

# Linear Regression Training
log_reg = reg.fit(x_train_scaled, y_train)
pkl.dump(log_reg, open("model/log_reg.model", 'wb'))
print "Trained Linear Regression"

#SVR RBF Kernel Training
svr_rbf_clf = svr_rbf.fit(x_train_scaled, y_train)
pkl.dump(svr_rbf_clf, open("model/svr_rbf_clf.model", 'wb'))
print "Trained RBF SVR"

#SVR Linear Kernel Training
svr_lin_clf = svr_lin.fit(x_train_scaled, y_train)
pkl.dump(svr_lin_clf, open("model/svr_lin_clf.model", 'wb'))
print "Trained Linear SVR"

#SVR Polynomial Kernel Training
svr_poly_clf = svr_poly.fit(x_train_scaled, y_train)
pkl.dump(svr_poly_clf, open("model/svr_poly_clf.model", 'wb'))
print "Trained Polynomial SVR"

#Decision Tree Training
dtree_reg = dtree.fit(x_train_scaled, y_train)
pkl.dump(dtree_reg, open("model/dtree_reg.model", 'wb'))
print "Trained Decision tree"