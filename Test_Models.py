import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import json
import ast

from sklearn import preprocessing
from sklearn.cross_validation import train_test_split
from sklearn.metrics import mean_squared_error

from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn import tree

import pickle as pkl

datafile = open('train_data/dataset.pkl', 'r')
x_train_scaled, x_test_scaled, y_train, y_test = pkl.load(datafile)

x_train_scaled, x_test_scaled, y_train, y_test = train_test_split(x_train_scaled, y_train, test_size=0.05, random_state=2)
y_test = y_test.astype(float)

plt.xlabel('Movies')
plt.ylabel('Ratings')

plt.axhline(y=0.0, color='black', linestyle='-')

# Linear Regression Training
log_reg = pkl.load(open('model/log_reg.model', 'r'))

y_pred = log_reg.predict(x_test_scaled)
y_pred[y_pred > 10.0] = 10.0
y_pred[y_pred < 0.0] = 0.0
testScore = mean_squared_error(y_pred, y_test)
print "Linear Regression : ", testScore

y_diff = y_pred - y_test
plt.plot(y_diff[1:100], color='blue', linewidth=1, label='Linear Regression')

#SVR RBF Kernel Training
svr_rbf_clf = pkl.load(open('model/svr_rbf_clf.model', 'r'))

y_pred = svr_rbf_clf.predict(x_test_scaled)
y_pred[y_pred > 10.0] = 10.0
y_pred[y_pred < 0.0] = 0.0
testScore_rbf = mean_squared_error(y_pred, y_test)
print "SVR RBF Kernel : ", testScore_rbf

y_diff = y_pred - y_test
plt.plot(y_diff[1:100], color='red', linewidth=1, label='SVR RBF Kernel')

#SVR Linear Kernel Training
svr_lin_clf = pkl.load(open('model/svr_lin_clf.model', 'r'))

y_pred = svr_lin_clf.predict(x_test_scaled)
y_pred[y_pred > 10.0] = 10.0
y_pred[y_pred < 0.0] = 0.0
testScore_lin = mean_squared_error(y_pred, y_test)
print "SVR Linear Kernel : ", testScore_lin

y_diff = y_pred - y_test
plt.plot(y_diff[1:100], color='orange', linewidth=1, label='SVR Linear Kernel')

#SVR Polynomial Kernel Training
svr_poly_clf = pkl.load(open('model/svr_poly_clf.model', 'r'))

y_pred = svr_poly_clf.predict(x_test_scaled)
y_pred[y_pred > 10.0] = 10.0
y_pred[y_pred < 0.0] = 0.0
testScore_poly = mean_squared_error(y_pred, y_test)
print "SVR Polynomial Kernel : ", testScore_poly

y_diff = y_pred - y_test
plt.plot(y_diff[1:100], color='yellow', linewidth=1, label='SVR Polynomial Kernel')

#Decision Tree Training
dtree_reg = pkl.load(open('model/dtree_reg.model', 'r'))

y_pred = dtree_reg.predict(x_test_scaled)
y_pred[y_pred > 10.0] = 10.0
y_pred[y_pred < 0.0] = 0.0
testScore = mean_squared_error(y_pred, y_test)
print "Decision Tree : ",testScore

y_diff = y_pred - y_test
plt.plot(y_diff[1:100], color='green', linewidth=1, label='Decision Tree Regression')

plt.legend()
plt.title('Movie Data Ratings')
plt.savefig('results_seen.png')
plt.show()