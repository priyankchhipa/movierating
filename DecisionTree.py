
# coding: utf-8

# ## Import Libraries
# Import the necessary Python libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import json
import ast

from sklearn import preprocessing
from sklearn import tree
from sklearn.metrics import mean_squared_error
from sklearn.cross_validation import train_test_split

data = pd.read_csv('train_final.csv', dtype='unicode')
clf = tree.DecisionTreeRegressor()

labels = data['vote_average']
train1 = data.drop(['vote_average', 'movie_id'], axis=1)

x_train, x_test, y_train, y_test = train_test_split(train1, labels, test_size=0.10, random_state=2)

scaler = preprocessing.StandardScaler().fit(x_train)
x_train_scaled = scaler.transform(x_train)

clf = clf.fit(x_train, y_train)

y_pred = clf.predict(x_test)
testScore = mean_squared_error(y_pred, y_test)
#testScore = clf.score(x_test, y_test)
print testScore
